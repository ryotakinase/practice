package practice8;

public class Question2 {

	public static void main(String[] args) {
		Cube c = new Cube(20);

		System.out.println("一辺" + c.line +"の一面の面積は" + area(c.line));
		System.out.println("一辺" + c.line + "の立方体の体積は" + volume(c.line));
	}

	private static int area(int a) {
		return a * a;
	}

	private static int volume(int a) {
		return a * a * a;
	}

}

class Cube {
	int line;

	Cube(int line) {
		this.line = line;
	}
}
