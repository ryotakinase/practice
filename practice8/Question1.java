package practice8;

public class Question1 {

	public static void main(String[] args) {
		Human tarao = new Human("タラちゃん", 3);
		Human sazae = new Human("サザエさん", 24);

		int birth = subtraction(sazae.age, tarao.age);

		System.out.println("サザエさんはタラちゃんを" + birth + "歳の時に出産しました");



	}

	private static int subtraction(int a, int b) {
		return a - b;
	}


}

class Human {
	String nickname;
	int age;

	public Human(String nickname, int age) {
		this.nickname = nickname;
		this.age = age;
	}
}