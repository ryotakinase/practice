package practice12;

import java.util.HashMap;

public class Question3 {
	public static void main(String[] args) {
		HashMap<String, Integer>branches = new HashMap<String, Integer>();

		branches.put("国語", 87);
		branches.put("数学", 81);
		branches.put("社会", 94);
		branches.put("理科", 79);
		branches.put("英語", 85);

		for(String key : branches.keySet()) {
			if(branches.get(key) <= 90) {
				int score = branches.get(key) + 10;
				branches.put(key, score);
			}
		}

		for(String key : branches.keySet()) {
			System.out.println(key + "の点数は" + branches.get(key) + "点です。");
		}
	}

}
