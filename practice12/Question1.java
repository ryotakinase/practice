package practice12;

import java.util.HashMap;

public class Question1 {
	public static void main(String[] args) {
		HashMap<String, String> fukuoka = new HashMap<String, String>();

		fukuoka.put("社名", "ALH株式会社");
		fukuoka.put("福岡営業所", "福岡県福岡市");

		System.out.println(fukuoka.get("社名"));
		System.out.println(fukuoka.get("福岡営業所"));
		
		HashMap<String, String> yokohama = new HashMap<String, String>();

		yokohama.put("社名", "ALH株式会社");
		yokohama.put("横浜営業所", "神奈川県横浜市");

		System.out.println(yokohama.get("社名"));
		System.out.println(yokohama.get("横浜営業所"));

	}

}
