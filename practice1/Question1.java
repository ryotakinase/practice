package practice1;

public class Question1 {
	public static void main(String[] args) {
		int num = 3;
		int result = 0;

		//加算
		result = 6 + num;
		System.out.println(6 + "+" + num + "=" + result);

		//減算
		result = 6 - num;
		System.out.println(6 + "-" + num + "=" + result);

		//乗算
		result = 6 * num;
		System.out.println(6 + "x" + num + "=" + result);

		//除算
		result = 6 / num;
		System.out.println(6 + "÷" + num + "=" + result);

		//乗除
		result = 6 % num;
		System.out.println(6 + "÷" + num + "の余りは" + result);
	}
}
