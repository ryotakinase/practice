package practice1;

public class Question2 {
	public static void main(String[] args) {
		int num = 3;
		int incrementResult = 0;
		int decrementResult = 0;

		//1加算
//		i = 1;
//		j = ++i; jは2

//		i = 1;
//		j = i++; jは1

		incrementResult = ++num;
		System.out.println("3に1加算した数は" + incrementResult);

		num = 3;

		//1減算
		decrementResult = --num;
		System.out.println("3から1減算した数は" + decrementResult);
	}
}
