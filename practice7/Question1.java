package practice7;

public class Question1 {
	public static void main(String[] args) {

		Human h = new Human();
		h.nickname = "あいうえお";
		h.age = 100;

		System.out.println(h.nickname + "は" + h.age + "歳です");

	}

}

class Human {
	String nickname;
	int age;
	double height;
	double weight;
}
