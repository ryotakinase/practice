package practice7;

public class Question2 {

	public static void main(String[] args) {
		Human h = new Human();
		h.nickname = "あいうえお";
		h.age = 100;
		h.height = 170.8;
		h.weight = 60.5;

		System.out.println(h.nickname + "は" + h.age + "歳です");
		System.out.println("身長は" + h.height + "  体重は" + h.weight);

	}

}
