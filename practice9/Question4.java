package practice9;

public class Question4 {

	public static void main(String[] args) {
		String numberOnly = "0231445145113123";
		String regex = "^[0-9].+$";

		if(numberOnly.matches(regex)) {
			System.out.println("「" + numberOnly + "」は数字だけで構成されています。");
		}

	}

}
