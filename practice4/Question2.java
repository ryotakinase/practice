package practice4;

public class Question2 {
	public static void main(String[] args) {
		int[] num = { 1, 9, 93, 19, 11, 76, 9, 66, 44, 33};

		int maxValue = 0;

		for(int i = 0; i < num.length; i++) {
			maxValue = Math.max(maxValue, num[i]);
		}

		System.out.println(maxValue);
	}

}
