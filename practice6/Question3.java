package practice6;

public class Question3 {
	public static void main(String[] args) {
		int front = 10;
		int back = 3;

		int product = multiplication(front, back);
		System.out.println(front + "*" + back + "=" + product);

		double quotient = division(front, back);
		System.out.println(front + "/" + back + "=" + quotient);
	}

	private static int multiplication(int f, int b) {
		return f * b;
	}

	private static double division(int f, int b) {
		return f / b;
	}

}
