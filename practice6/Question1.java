package practice6;

public class Question1 {
	public static void main(String[] args) {
		countDown();
		countUp();
	}


	private static void countDown() {
		System.out.println("カウントダウン");
		for(int i = 5; i >= 0; i--) {
			System.out.println(i);
		}
	}

	private static void countUp() {
		System.out.println("カウントアップ");
		for(int i = 0; i <= 5; i++) {
			System.out.println(i);
		}
	}

}
