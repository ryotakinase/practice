package practice6;

public class Question4 {
	public static void main(String[] args) {
		int x = 10;
		int y = 20;
		int z = 30;

		System.out.println(minValue(x, minValue(y, z)));
	}

	private static int minValue(int a, int b) {
		return Math.min(a, b);
	}

}
