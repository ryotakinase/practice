package practice6;

public class Question2 {
	public static void main(String[] args) {
		int start = 10;
		countDown(start);
		countUp(start);
	}

	private static void countDown(int s) {
		System.out.println("カウントダウン");
		for(int i = s; i >= 0; i--) {
			System.out.println(i);
		}
	}

	private static void countUp(int s) {
		System.out.println("カウントアップ");
		for(int i = 1; i <= s; i++) {
			System.out.println(i);
		}
	}

}
