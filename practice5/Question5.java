package practice5;

import java.util.Arrays;

public class Question5 {
	public static void main(String[] args) {
		int[] values = {32, 23, 56, 12, 63, 33, 28, 91, 75, 43, 51};

		Arrays.sort(values);

		for(int num : values) {
			System.out.println(num);
		}
	}

}
