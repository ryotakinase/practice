package practice5;

public class Question2 {

	public static void main(String[] args) {
		int[] score = {0, 1, 2, 1, 0, 3, 0, 1, 0, 1, 0, 1, 0, 0, 2, 1, 3, 0};

		int giants = 0;
		int tigers = 0;

		for(int i = 0; i < score.length; i++) {
			if(i % 2 == 0) {
				giants += score[i];
			} else {
				tigers += score[i];
			}
		}

		System.out.println("巨人の得点：" + giants);
		System.out.println("阪神の得点：" + tigers);
		if(giants > tigers) {
			System.out.println("巨人の勝利");
		} else if (giants == tigers) {
			System.out.println("引き分け");
		} else {
			System.out.println("阪神の勝利");
		}

	}

}
