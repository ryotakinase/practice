package practice3;

public class Question2 {

	public static void main(String[] args) {
		int year = 2016;

		if((year % 4 == 0) || (year % 2 == 0)) {
			System.out.println(year + "年は夏季もしくは冬季オリンピックです");
		} else if(!(year % 4 == 0) && !(year % 2 == 0)) {
			System.out.println(year + "年はオリンピックではありません");
		}
	}

}
