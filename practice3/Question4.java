package practice3;

public class Question4 {

	public static void main(String[] args) {
		int middleScore = 100;
		int finalScore = 10;
		int totalScore = middleScore + finalScore;

		if(middleScore >= 60 && finalScore >= 60) {
			System.out.println("合格");
		} else if(totalScore >= 130) {
			System.out.println("合格");
		} else if(totalScore >= 100 && middleScore >= 90 || finalScore >= 90) {
			System.out.println("合格");
		} else {
			System.out.println("不合格");
		}

	}

}
