package practice2;

public class Question4 {
	public static void main(String[] args) {
		int currentMonth = 9;
		System.out.println("現在は" + currentMonth + "月です。");

		switch(currentMonth) {
		case 2:
		case 4:
		case 6:
		case 9:
		case 11:
			System.out.println("31日はありません");
			break;
		default:
			System.out.println("31日まであります");
		}

		switch(currentMonth) {
		case 5:
		case 6:
		case 7:
		case 8:
			System.out.println("Rは入りません");
			break;
		default:
			System.out.println("Rが入ります。");
		}
	}

}
