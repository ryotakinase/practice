package practice2;

public class Question3 {
	public static void main(String[] args) {
		int money = 999;
		int product = 140;
		int change = money - product;

		System.out.println(product + "円の商品を" + money + "円で購入した場合のおつりは・・・");

		if(change / 500 >= 1) {
			System.out.println("500円玉は" + (change / 500) + "枚");
			change %= 500;
		}

		if(change / 100 >= 1) {
			System.out.println("100円玉は" + (change / 100) + "枚");
			change %= 100;
		}

		if(change / 50 >= 1) {
			System.out.println("50円玉は" + (change / 50) + "枚");
			change %= 50;
		}

		if(change / 10 >= 1) {
			System.out.println("10円玉は" + (change / 10) + "枚");
			change %= 10;
		}

		if(change / 5 >= 1) {
			System.out.println("5円玉は" + (change / 5) + "枚");
			change %= 5;
		}

		if(change / 1 >= 1) {
			System.out.println("1円玉は" + (change / 1) + "枚");

		}


	}

}
