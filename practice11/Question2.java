package practice11;

import java.util.ArrayList;

public class Question2 {
	public static void main(String[] args) {
		ArrayList<Integer> values = new ArrayList<Integer>();

		values.add(50);
		values.add(55);
		values.add(70);
		values.add(65);
		values.add(80);


		for(int i = 0; i < values.size(); i++) {
			if(i % 2 == 0) {
				values.set(i, 0);
			}
		}

		for(int i = 0; i < values.size(); i++) {
			System.out.println(values.get(i));
		}
	}

}
